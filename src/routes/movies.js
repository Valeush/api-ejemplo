const { Router } = require('express');
const router = Router();

const movies = require('../sample.json');


router.get('/', (req, res) => {
    res.json(movies);
})

router.post('/', (req ,res) =>{
    const {title, director, year, rating} = req.body;
    if (title && director && year && rating){
        const id = movies.length + 1;
        const newMovie = {...req.body, id};
        movies.push(newMovie);
        res.json(movies);
    }else{
        res.status(500).json({error: 'there was an error.'});
    }   
})

router.put('/:id', (req, res) =>{
    const id = parseInt(req.params.id);
    const {title, director, year, rating} = req.body;
    if (title && director && year && rating){
        for (var i = 0; i < movies.length; i++){
            if (movies[i].id == id){
                movies[i].title = title;
                movies[i].director = director;
                movies[i].year = year;
                movies[i].rating = rating;
            }
        }
        res.json(movies);
    }else{
        res.status(500).json({error: 'there was an error.'});
    }
})



router.delete('/:id', (req, res) =>{
    const id = parseInt(req.params.id);
    for (var i = 0; i < movies.length; i++){
        if (movies[i].id == id){
            movies.splice(i, 1);
        }
    }
res.send(movies);
})

module.exports = router;